package model.data_structures;

public class ArregloDinamico <T extends Comparable<T>> implements IArregloDinamico<T> 
{
	/**
	 * Capacidad maxima del arreglo
	 */
    private int tamanoMax;
	/**
	 * Numero de elementos presentes en el arreglo (de forma compacta desde la posicion 0)
	 */
    private int tamanoAct;
    /**
     * Arreglo de elementos de tamaNo maximo
     */
    private T elementos[ ];

    /**
     * Construir un arreglo con la capacidad maxima inicial.
     * @param max Capacidad maxima inicial
     */
	@SuppressWarnings("unchecked")
	public ArregloDinamico( int max )
    {
           elementos = (T[]) new Object[max];
           tamanoMax = max;
           tamanoAct = 0;
    }
    
	@SuppressWarnings("unchecked")
	public void agregar( T dato )
    {
           if ( tamanoAct == tamanoMax )
           {  // caso de arreglo lleno (aumentar tamaNo)
                tamanoMax = tamanoMax + (10*tamanoMax);
                T [ ] copia = elementos;
                elementos = (T[]) new Object[tamanoMax];
                for ( int i = 0; i < tamanoAct; i++)
                {
                 	 elementos[i] = copia[i];
                } 
        	    System.out.println("Arreglo lleno: " + tamanoAct + " - Arreglo aumentado: " + tamanoMax);
           }	
           elementos[tamanoAct] = dato;
           tamanoAct++;
   }

	public int darCapacidad() {
		return tamanoMax;
	}

	public int darTamano() {
		return tamanoAct;
	}

	public T darElemento(int i) {
		// TODO implementar
		return elementos[i];
	}

	public T buscar(T dato) {
		// TODO implementar
		// Recomendacion: Usar el criterio de comparacion natural (metodo compareTo()) definido en Ts.
		T rta =null;
		for (int i = 0; i < elementos.length; i++)
		{
			if(elementos[i]==null)
			{
				break;
			}
			if(dato.compareTo(elementos[i])==0)
			{
				rta = elementos[i];
				break;
			}
		}
		return rta;
	}
	
	public void cambiar(T dato, int i)
	{
		elementos[i] = dato;
	}


	
	@SuppressWarnings("unchecked")
	public T eliminar(T dato) 
	{
		// TODO implementar
		
		// Recomendacion: Usar el criterio de comparacion natural (metodo compareTo()) definido en Ts.
        T rta = null;
		T [ ] copia = (T[]) new Object[tamanoMax];
        int h = 0;
		for (int i = 0; i < elementos.length; i++)
		{
			if(elementos[i]==null)
			{
				break;
			}
			if(dato.compareTo(elementos[i])!=0)
			{
				copia[h]= elementos[i];
				h++;
				
			}
			else
			{
				tamanoAct--;
				rta = elementos[i];
			}
		}
		elementos = copia;
		return rta;
	}
}
