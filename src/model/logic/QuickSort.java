package model.logic;

import model.data_structures.ArregloDinamico;

/**
 *  The {@code Quick} class provides static methods for sorting an
 *  array and selecting the ith smallest element in an array using quicksort.
 *  <p>
 *  For additional documentation,
 *  see <a href="https://algs4.cs.princeton.edu/23quick">Section 2.3</a> of
 *  <i>Algorithms, 4th Edition</i> by Robert Sedgewick and Kevin Wayne.
 *
 *  @author Robert Sedgewick
 *  @author Kevin Wayne
 */
public class QuickSort<T extends Comparable<T>>
{

    // This class should not be instantiated.
    public QuickSort() { }

    /**
     * Rearranges the array in ascending order, using the natural order.
     * @param a the array to be sorted
     */
    public void sort(ArregloDinamico<T> a) 
    {
    	StdRandom<T> std= new StdRandom<T>();
        std.shuffle(a);
        sort(a, 0, a.darTamano() - 1);
        assert isSorted(a);
    }

    // quicksort the subarray from a[lo] to a[hi]
    private void sort(ArregloDinamico<T> a, int lo, int hi) { 
        if (hi <= lo) return;
        int j = partition(a, lo, hi);
        sort(a, lo, j-1);
        sort(a, j+1, hi);
        assert isSorted(a, lo, hi);
    }

    // partition the subarray a[lo..hi] so that a[lo..j-1] <= a[j] <= a[j+1..hi]
    // and return the index j.
    private int partition(ArregloDinamico<T> a, int lo, int hi) {
        int i = lo;
        int j = hi + 1;
        T v = a.darElemento(lo);
        while (true) { 

            // find item on lo to swap
            while (less(a.darElemento(i++), v)) {
                if (i == hi) break;
            }

            // find item on hi to swap
            while (less(v, a.darElemento(--j))) {
                if (j == lo) break;      // redundant since a[lo] acts as sentinel
            }

            // check if pointers cross
            if (i >= j) break;

            exch(a, i, j);
        }

        // put partitioning item v at a[j]
        exch(a, lo, j);

        // now, a[lo .. j-1] <= a[j] <= a[j+1 .. hi]
        return j;
    }

    /**
     * Rearranges the array so that {@code a[k]} contains the kth smallest key;
     * {@code a[0]} through {@code a[k-1]} are less than (or equal to) {@code a[k]}; and
     * {@code a[k+1]} through {@code a[n-1]} are greater than (or equal to) {@code a[k]}.
     *
     * @param  a the array
     * @param  k the rank of the key
     * @return the key of rank {@code k}
     * @throws IllegalArgumentException unless {@code 0 <= k < a.length}
     */
    public T select(ArregloDinamico<T> a, int k) {
        if (k < 0 || k >= a.darTamano())
        {
            throw new IllegalArgumentException("index is not between 0 and " + a.darTamano() + ": " + k);
        }
        StdRandom<T> std= new StdRandom<T>();
        std.shuffle(a);
        int lo = 0, hi = a.darTamano() - 1;
        while (hi > lo) {
            int i = partition(a, lo, hi);
            if      (i > k) hi = i - 1;
            else if (i < k) lo = i + 1;
            else return a.darElemento(i);
        }
        return a.darElemento(lo);
    }



   /***************************************************************************
    *  Helper sorting functions.
    ***************************************************************************/
    
    // is v < w ?
    private boolean less(T v, T w)
    {
        if (v == w) return false;   // optimization when reference equals
        return v.compareTo(w) < 0;
    }
        
    // exchange a[i] and a[j]
    private void exch(ArregloDinamico<T> a, int i, int j)
	{
		T swap =a.darElemento(i);

		T seg =a.darElemento(j);
		a.cambiar(seg, i);
		a.cambiar(swap, j);
	}


   /***************************************************************************
    *  Check if array is sorted - useful for debugging.
    ***************************************************************************/
    private boolean isSorted(ArregloDinamico<T> a) {
        return isSorted(a, 0, a.darTamano() - 1);
    }

    private boolean isSorted(ArregloDinamico<T> a, int lo, int hi) {
        for (int i = lo + 1; i <= hi; i++)
            if (less(a.darElemento(i), a.darElemento(i-1))) return false;
        return true;
    }


}

