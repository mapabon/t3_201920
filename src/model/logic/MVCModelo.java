package model.logic;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import com.opencsv.CSVReader;

import model.data_structures.ArregloDinamico;

/**
 * Definicion del modelo del mundo
 *
 */
public class MVCModelo 
{

	/**
	 * Atributos del modelo del mundo
	 */
	private ArregloDinamico<UBERTrip> datos;

	private int cont;


	private ShellSort<UBERTrip> shell;

	private MergeSort<UBERTrip> merge;

	private QuickSort<UBERTrip> quick;

	private ArregloDinamico<UBERTrip> punto3;

	private int hora;


	/**
	 * Constructor del modelo del mundo con capacidad predefinida
	 */
	public MVCModelo()
	{
		datos = new ArregloDinamico<UBERTrip>(10000);
		shell = new ShellSort<>();
		merge = new MergeSort<>();
		quick = new QuickSort<>();
		cont = 0;

	}

	public void carga()
	{
		CSVReader reader = null;

		try 
		{
			reader = new CSVReader(new FileReader("./data/bogota-cadastral-2018-2-All-HourlyAggregate.csv"));

			for(String[] nextLine : reader) 
			{
				if(nextLine[0].contains("source")){}
				else
				{
					UBERTrip v = new UBERTrip(Integer.parseInt(nextLine[0]), Integer.parseInt(nextLine[1]), Integer.parseInt(nextLine[2]), 
							Double.parseDouble(nextLine[3]), Double.parseDouble(nextLine[4]), Double.parseDouble(nextLine[5]), Double.parseDouble(nextLine[6]));
					cont++;
					datos.agregar(v);
				}
			}
		} 
		catch (FileNotFoundException e) {e.printStackTrace();} 

		finally
		{
			if (reader != null) 
			{
				try {reader.close();} 
				catch (IOException e) {e.printStackTrace();}
			}
		}
	}

	public ArregloDinamico<UBERTrip> getArreglo()
	{
		return datos;
	}

	public int getHora() {
		return hora;
	}

	public void setHora(int hora) {
		this.hora = hora;
	}

	public int darContador()
	{
		return cont;
	}



	public ArregloDinamico<UBERTrip> consultaViajesHora(int hora)
	{

		ArregloDinamico<UBERTrip> lista = new ArregloDinamico<>(10000);
		for (int i = 0; i < datos.darTamano(); i++)
		{
			if(datos.darElemento(i).getHour()==hora)
			{
				lista.agregar(datos.darElemento(i));
			}
		}
		punto3 = lista;
		return punto3;
	}

	public ArregloDinamico<UBERTrip> shellSort()
	{	if(punto3.darTamano()!=0)
		shell.sort(punto3);

	else
	{
		System.out.println("Error. No se ha inicializado el arreglo a ordenar");
	}
	return punto3;
	}

	public ArregloDinamico<UBERTrip> mergeSort()
	{
		if(punto3.darTamano()!=0)
			merge.sort(punto3);
		else
			System.out.println("Error. No se ha inicializado el arreglo a ordenar");
		return punto3;
	}

	public ArregloDinamico<UBERTrip> quickSort()
	{
		if(punto3.darTamano()!=0)
			quick.sort(punto3);
		else
			System.out.println("Error. No se ha inicializado el arreglo a ordenar");
		return punto3;
	}



}
