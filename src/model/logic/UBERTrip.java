package model.logic;

public class UBERTrip implements Comparable<UBERTrip>
{
	
	//----------------------------------------------------------------------------------------------------------------------------------
		// Attributes
		//----------------------------------------------------------------------------------------------------------------------------------

		private int sourceid;
		private int dstid;
		private int hour;
		private double mean_travel_time;
		private double standard_deviation_travel_time;
		private double geometric_mean_travel_time;
		private double geometric_standard_deviation_travel_time;


		//----------------------------------------------------------------------------------------------------------------------------------
		// Constructors
		//----------------------------------------------------------------------------------------------------------------------------------
	

		//Constructor for Hora
		public UBERTrip(int pSource, int pDstid, int pHour, double pMean_travel_time, double pStandard_deviation_travel_time, 
				double pGeometric_mean_travel_time, double pGeometric_standard_deviation_travel_time)
		{
			setSourceid(pSource);
			setDstid(pDstid);
			setHour(pHour);
			setMean_travel_time(pMean_travel_time);
			setStandard_deviation_travel_time(pStandard_deviation_travel_time);
			setGeometric_mean_travel_time(pGeometric_mean_travel_time);
			setGeometric_standard_deviation_travel_time(pGeometric_standard_deviation_travel_time);
		}


		//----------------------------------------------------------------------------------------------------------------------------------
		// Methods
		//----------------------------------------------------------------------------------------------------------------------------------

		//Source ID
		public int getSourceid() {
			return sourceid;
		}

		public void setSourceid(int sourceid) {
			this.sourceid = sourceid;
		}

		//Dst ID
		public int getDstid() {
			return dstid;
		}

		public void setDstid(int dstid) {
			this.dstid = dstid;
		}

		//Hour
		public int getHour() {
			return hour;
		}

		public void setHour(int hour) {
			this.hour = hour;
		}

		//Mean Travel Time
		public double getMean_travel_time() {
			return mean_travel_time;
		}

		public void setMean_travel_time(double mean_travel_time) {
			this.mean_travel_time = mean_travel_time;
		}

		//Standard Deviation Travel Time
		public double getStandard_deviation_travel_time() {
			return standard_deviation_travel_time;
		}

		public void setStandard_deviation_travel_time(double standard_deviation_travel_time) {
			this.standard_deviation_travel_time = standard_deviation_travel_time;
		}

		//Geometric Mean Travel Time
		public double getGeometric_mean_travel_time() {
			return geometric_mean_travel_time;
		}

		public void setGeometric_mean_travel_time(double geometric_mean_travel_time) {
			this.geometric_mean_travel_time = geometric_mean_travel_time;
		}

		//Geometric Standard Deviation Travel Time
		public double getGeometric_standard_deviation_travel_time() {
			return geometric_standard_deviation_travel_time;
		}

		public void setGeometric_standard_deviation_travel_time(double geometric_standard_deviation_travel_time) {
			this.geometric_standard_deviation_travel_time = geometric_standard_deviation_travel_time;
		}

	@Override
	public int compareTo(UBERTrip uber)
	{
		if(mean_travel_time>uber.getMean_travel_time())
			return 1;
		else if(mean_travel_time<uber.getMean_travel_time())
			return -1;
		else
		{
			if(standard_deviation_travel_time>uber.getStandard_deviation_travel_time())
				return 1;
			else if (standard_deviation_travel_time<uber.getStandard_deviation_travel_time())
				return -1;
			else 
				return 0;
		}
	}

}
