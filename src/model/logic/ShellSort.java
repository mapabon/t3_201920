package model.logic;

import model.data_structures.ArregloDinamico;

public class ShellSort<T extends Comparable<T>>
{
	public void sort(ArregloDinamico<T> a)
	{
		int N = a.darTamano();
		int h = 1;
		while (h < N/3)
		{
			h = 3*h +1;
		}
		while ( h>= 1)
		{
			for (int i = h; i < N; i++)
			{
				for (int j=i; j>=h && less(a.darElemento(j), a.darElemento(j-h)); j-=h)
				{
					exch(a,j,j-h);
				}
			}
			h = h/3;
		}
	}
	
	private boolean less(T v, T w)
	{
		return v.compareTo(w)<0;
	}
	
	private void exch(ArregloDinamico<T> a, int i, int j)
	{
		T swap =a.darElemento(i);

		T seg =a.darElemento(j);
		a.cambiar(seg, i);
		a.cambiar(swap, j);
	}
}
