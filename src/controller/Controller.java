package controller;

import java.util.Scanner;

import model.data_structures.ArregloDinamico;
import model.logic.MVCModelo;
import model.logic.UBERTrip;
import view.MVCView;

public class Controller {

	/* Instancia del Modelo*/
	private MVCModelo modelo;

	/* Instancia de la Vista*/
	private MVCView view;

	/**
	 * Crear la vista y el modelo del proyecto
	 * @param capacidad tamaNo inicial del arreglo
	 */
	public Controller ()
	{
		view = new MVCView();
		modelo = new MVCModelo();
	}

	public void run() 
	{
		Scanner lector = new Scanner(System.in);
		boolean fin = false;
		String dato = "";
		String respuesta = "";

		while( !fin ){
			view.printMenu();

			int option = lector.nextInt();
			switch(option){
			case 1:
				modelo = new MVCModelo();
				modelo.carga();
				System.out.println("Archivos cargados con exito.");
				System.out.println("Numero de viajes leidos: " + modelo.darContador() + "\n---------");	
				for (int i = 0; i < modelo.darContador(); i++) 
				{
					if (i == 0)
					{
						System.out.println("Datos primer viaje");
						System.out.println("Zona origen: " + modelo.getArreglo().darElemento(i).getSourceid());	
						System.out.println("Zona destino: " + modelo.getArreglo().darElemento(i).getDstid());
						System.out.println("Hora de viaje: " + modelo.getArreglo().darElemento(i).getHour());	
						System.out.println("Tiempo promedio de Viaje: " + modelo.getArreglo().darElemento(i).getMean_travel_time());	
						System.out.println("");

					}
					if ( i == modelo.darContador()-1 )
					{
						System.out.println("Datos ultimo viaje");
						System.out.println("Zona origen: " + modelo.getArreglo().darElemento(i).getSourceid());	
						System.out.println("Zona destino: " + modelo.getArreglo().darElemento(i).getDstid());
						System.out.println("Hora de viaje: " + modelo.getArreglo().darElemento(i).getHour());	
						System.out.println("Tiempo promedio de Viaje: " + modelo.getArreglo().darElemento(i).getMean_travel_time());
						System.out.println("");
					}
				}

				break;

			case 2: 
				System.out.println("--------- \nDar hora deseada (XX): ");
				int hora = lector.nextInt();
				ArregloDinamico<UBERTrip> arreglo = modelo.consultaViajesHora(hora);
				System.out.println("El número de viajes resultantes de la consulta fue de: " + arreglo.darTamano());
				System.out.println("");
				break;	

			case 3:
				long startTime = System.currentTimeMillis(); 
				ArregloDinamico<UBERTrip> rta = modelo.shellSort();
				long endTime = System.currentTimeMillis();
				long duration = endTime - startTime;
				System.out.println("Datos ordenados con exito, haciendo uso del algoritmo de ordenamiento ShellSort.");
				System.out.println("El tiempo en milisegundos que tomó el algoritmo haciendo el ordenamiento fue de: " + duration + " milisegundos \n---------");

				System.out.println("Los primeros 10 viajes ordenados son: " + "\n---------");
				for (int i = 0; i < rta.darTamano(); i++)
				{
					if (i<10)
					{
						System.out.println("Viaje: " + i);	
						System.out.println("Zona origen: " + rta.darElemento(i).getSourceid());	
						System.out.println("Zona destino: " + rta.darElemento(i).getDstid());
						System.out.println("Tiempo promedio de Viaje: " + rta.darElemento(i).getHour());
						System.out.println("Desviacion estandar: " + rta.darElemento(i).getStandard_deviation_travel_time());
					}
				}

				System.out.println("\n---------" + "Los primeros 10 viajes ordenados son: " + "\n---------");
				for (int i = 0; i < rta.darTamano(); i++)
				{
					if (i>rta.darTamano()-10)
					{
						System.out.println("Viaje: " + i);	
						System.out.println("Zona origen: " + rta.darElemento(i).getSourceid());	
						System.out.println("Zona destino: " + rta.darElemento(i).getDstid());
						System.out.println("Tiempo promedio de Viaje: " + rta.darElemento(i).getHour());
						System.out.println("Desviacion estandar: " + rta.darElemento(i).getStandard_deviation_travel_time());
					}
				}

			case 4:
				long startTime1 = System.currentTimeMillis();
				ArregloDinamico<UBERTrip> ans = modelo.mergeSort();
				long endTime1 = System.currentTimeMillis();
				long duration1 = endTime1 - startTime1;
				System.out.println("Datos ordenados con exito, haciendo uso del algoritmo de ordenamiento MergeSort.");
				System.out.println("El tiempo en milisegundos que tomó el algoritmo haciendo el ordenamiento fue de: " + duration1 + " milisegundos \n---------");

				System.out.println("Los primeros 10 viajes ordenados son: " + "\n---------");
				for (int i = 0; i < ans.darTamano(); i++)
				{
					if (i<10)
					{
						System.out.println("Viaje: " + i);	
						System.out.println("Zona origen: " + ans.darElemento(i).getSourceid());	
						System.out.println("Zona destino: " + ans.darElemento(i).getDstid());
						System.out.println("Tiempo promedio de Viaje: " + ans.darElemento(i).getHour());
						System.out.println("Desviacion estandar: " + ans.darElemento(i).getStandard_deviation_travel_time());
					}
				}

				System.out.println("\n---------" + "Los primeros 10 viajes ordenados son: " + "\n---------");
				for (int i = 0; i < ans.darTamano(); i++)
				{
					if (i>ans.darTamano()-10)
					{
						System.out.println("Viaje: " + i);	
						System.out.println("Zona origen: " + ans.darElemento(i).getSourceid());	
						System.out.println("Zona destino: " + ans.darElemento(i).getDstid());
						System.out.println("Tiempo promedio de Viaje: " + ans.darElemento(i).getHour());
						System.out.println("Desviacion estandar: " + ans.darElemento(i).getStandard_deviation_travel_time());
					}
				}
			case 5:
				long startTime2 = System.currentTimeMillis();
				ArregloDinamico<UBERTrip> x = modelo.mergeSort();
				long endTime2 = System.currentTimeMillis();
				long duration2 = endTime2 - startTime2;
				System.out.println("Datos ordenados con exito, haciendo uso del algoritmo de ordenamiento MergeSort.");
				System.out.println("El tiempo en milisegundos que tomó el algoritmo haciendo el ordenamiento fue de: " + duration2 + " milisegundos \n---------");

				System.out.println("Los primeros 10 viajes ordenados son: " + "\n---------");
				for (int i = 0; i < x.darTamano(); i++)
				{
					if (i<10)
					{
						System.out.println("Viaje: " + i);	
						System.out.println("Zona origen: " + x.darElemento(i).getSourceid());	
						System.out.println("Zona destino: " + x.darElemento(i).getDstid());
						System.out.println("Tiempo promedio de Viaje: " + x.darElemento(i).getHour());
						System.out.println("Desviacion estandar: " + x.darElemento(i).getStandard_deviation_travel_time());
					}
				}

				System.out.println("\n---------" + "Los primeros 10 viajes ordenados son: " + "\n---------");
				for (int i = 0; i < x.darTamano(); i++)
				{
					if (i>x.darTamano()-10)
					{
						System.out.println("Viaje: " + i);	
						System.out.println("Zona origen: " + x.darElemento(i).getSourceid());	
						System.out.println("Zona destino: " + x.darElemento(i).getDstid());
						System.out.println("Tiempo promedio de Viaje: " + x.darElemento(i).getHour());
						System.out.println("Desviacion estandar: " + x.darElemento(i).getStandard_deviation_travel_time());
					}
				}
			default: 
				System.out.println("--------- \n Opcion Invalida !! \n---------");
				break;
			}
		}

	}	
}
