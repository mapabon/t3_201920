package test.logic;

import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import model.data_structures.ArregloDinamico;
import model.logic.MergeSort;
import model.logic.QuickSort;
import model.logic.StdRandom;

public class TestQuickSort 
{
	private ArregloDinamico<Integer> arreglo;
	private static int TAMANO=33;
	private QuickSort quick;

	@Before
	public void setUp1() 
	{
		arreglo= new ArregloDinamico<Integer>(TAMANO);
		quick = new QuickSort<Integer>();
	}

	public void setUp2()
	{
		for(int i =0; i< TAMANO; i++){
			arreglo.agregar(i);
		}
		StdRandom<Integer> std= new StdRandom<Integer>();
		std.shuffle(arreglo);
	}

	public void setUp3()
	{
		for(int i =0; i< TAMANO; i++){
			arreglo.agregar(i);
		}
	}

	public void setUp4()
	{
		for(int i =TAMANO; i > 0; i--){
			arreglo.agregar(i);
		}
	}

	@Test
	public void desorden()
	{
		setUp2();
		quick.sort(arreglo);
		for(int i = 1; i < arreglo.darTamano(); i++)
		{
			assertTrue("No estan ordenados correctamente",arreglo.darElemento(i-1).compareTo(arreglo.darElemento(i))<0);
		}


	}

	@Test
	public void ordenadosAscendentemente()
	{
		setUp3();
		quick.sort(arreglo);
		for(int i = 1; i < arreglo.darTamano(); i++)
		{
			assertTrue("No estan ordenados correctamente",arreglo.darElemento(i-1).compareTo(arreglo.darElemento(i))<0);
		}
	}

	@Test
	public void ordenadosDescendentemente()
	{
		setUp4();
		quick.sort(arreglo);
		for(int i = 1; i < arreglo.darTamano(); i++)
		{
			assertTrue("No estan ordenados correctamente",arreglo.darElemento(i-1).compareTo(arreglo.darElemento(i))<0);
		}
	}
}
